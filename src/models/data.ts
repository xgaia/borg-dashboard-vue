export interface Data {
  // eslint-disable-next-line camelcase
  too_old: {
    count: number,
    servers: string[]
  },
  timestamp: number;
  servers: {
    [s: string]: DataServer
  }
}

export interface DataServer {
  // eslint-disable-next-line camelcase
  too_old: number,
  backups: number[]
}
