export default interface Server {
  name: string;
  tooOld: boolean;
  backups: number[];
  lastBackup: number;
}
