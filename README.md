# Borgbackup web dashboard

See <https://framagit.org/framasoft/borgbackup/borg-dashboard-exporter> to know how to use it.

See <https://framasoft.frama.io/borgbackup/borg-dashboard-vue> for a demo.

Get latest built on <https://framasoft.frama.io/borgbackup/borg-dashboard-vue/borg-dashboard-latest.zip>.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production

If you want to serve your dashboard on a different directory than `/`, please modify [vue.config.js](vue.config.js) before running this command.

```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## LICENSE
© 2019 Framasoft, GPLv3

See [LICENSE](LICENSE) file.
